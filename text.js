const topBarText = document.getElementById('top-bar-text');
const phrases = [
  'links schizo.gr',
  'apt install caddy',
  'echo schizo',
  'ping schizo.gr',
  'pkg install tor',
  'lsblk',
  'vim schizo.txt'
];

let phraseIndex = 0;
let letterIndex = 0;
let isTyping = true;
let waitToChange = 0;

function type() {
  if (isTyping) {
    topBarText.textContent = phrases[phraseIndex].slice(0, letterIndex) + '|';
    letterIndex++;

    if (letterIndex > phrases[phraseIndex].length) {
      isTyping = false;
      waitToChange = 20;
    }
  } else {
    waitToChange--;
    if (waitToChange <= 0) {
      letterIndex--;
      topBarText.textContent = phrases[phraseIndex].slice(0, letterIndex);

      if (letterIndex <= 0) {
        isTyping = true;
        phraseIndex++;
        if (phraseIndex >= phrases.length) {
          phraseIndex = 0;
        }
      }
    }
  }

  setTimeout(type, 100);
}

type();
